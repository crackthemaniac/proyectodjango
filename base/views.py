from django.shortcuts import render
from deuda.models import Pago, Deuda
from django.db.models import Sum
from django.contrib.auth.decorators import login_required

@login_required


def home(request):
	template_name = 'home.html'
	data = {}
	data['pagado'] = Pago.objects.aggregate(Sum('monto'))['monto__sum']
	data['deuda'] = Deuda.objects.aggregate(Sum('monto'))['monto__sum']
	
	return render(request, template_name, data)