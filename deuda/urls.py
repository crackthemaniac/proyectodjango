from django.urls import path
from . import views

urlpatterns = [
    path('agregar_pago', views.agregarPago, name="agregar_pago"),
    path('mis_deudas', views.misDeudas, name="mis_deudas"),
    path('pagar_deuda', views.pagarDeuda, name="pagar_deuda"),
    path('mis_pagos', views.misPagos, name="mis_pagos")
]