from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Categoria(models.Model):
	nombre =  models.CharField(max_length=144)

	def __str__(self):
		return self.nombre


class Pago(models.Model):
	producto = models.CharField(max_length=144)
	monto = models.IntegerField()
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	deudor = models.ForeignKey(User, on_delete=models.CASCADE)
	fecha = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.producto

class Deuda(models.Model):
	producto = models.CharField(max_length=144)
	monto = models.IntegerField()
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	pagador = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.producto

class pagoDeuda(models.Model):
	transaccion = models.IntegerField()
	comentario = models.TextField()

