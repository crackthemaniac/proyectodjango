from django import forms
from .models import Pago, Deuda, pagoDeuda

class PagoForm(forms.ModelForm):
	class Meta:
		model = Pago
		fields = [
		'producto',
		'monto',
		'categoria',
		'deudor',
		]
		widgets = {
		'producto': forms.TextInput(attrs={'class':'form-control'}),
		'monto': forms.TextInput(attrs={'class':'form-control'}),
		}


class DeudaForm(forms.ModelForm):
	class Meta:
		model = pagoDeuda
		fields = [
		'transaccion',
		'comentario',
		]